// format basic salary when type in input but can't validation if we use it :()
// document.querySelector("#luongCB").addEventListener(
//   "keyup",
//   function (evt) {
//     var n = parseInt(this.value.replace(/\D/g, ""), 10);
//     document.querySelector("#luongCB").value = n.toLocaleString();
//   },
//   false
// );
function getInforFromInput() {
  let account = document.querySelector("#tknv").value;
  let fullName = document.querySelector("#name").value;
  let email = document.querySelector("#email").value;
  let passWord = document.querySelector("#password").value;
  let workingDays = document.querySelector("#datepicker").value;
  let basicSalary = +document.querySelector("#luongCB").value;
  let position = document.querySelector("#chucvu").value;
  let workingHours = +document.querySelector("#gioLam").value;
  return (newStaff = new Staff(
    account,
    fullName,
    email,
    passWord,
    workingDays,
    basicSalary,
    position,
    workingHours
  ));
}
function showStaffList(nv) {
  let contentHTML = "";
  for (let i = 0; i < nv.length; i++) {
    let nhanVien = nv[i];
    let nhanVienPosition = nhanVien.position;
    function getPosition() {
      if (nhanVienPosition == "1") {
        return "Sếp";
      } else if (nhanVienPosition == "2") {
        return "Trưởng Phòng";
      } else {
        return "Nhân viên";
      }
    }
    let salaryFormatted = Intl.NumberFormat("vi-VI", {
      style: "currency",
      currency: "VND",
    }).format(`${nhanVien.fullSalary()}`);
    let contentTrTag = /*html*/ `<tr>
        <td>${nhanVien.account}</td>
        <td>${nhanVien.fullName}</td>
        <td>${nhanVien.email}</td>
        <td>${nhanVien.workingDays}</td>
        <td>${getPosition()}</td>
        <td>${salaryFormatted}</td>
        <td class="staffType">${nhanVien.type()}</td>
        <td>
        <button class="btn btn-success" style="width: 80px" data-toggle="modal"
        data-target="#myModal"onClick="editInfor('${
          nhanVien.account
        }')">Edit</button>
        <button class="btn btn-danger"  style="width: 80px" onClick="deleteStaff('${
          nhanVien.account
        }')">Delete</button>
    
        </td>
        </tr>`;
    contentHTML += contentTrTag;
  }
  document.querySelector("#tableDanhSach").innerHTML = contentHTML;
}

// function turnOffModal() {
//   //   document.querySelector("#myModal").style.display = "none";
//   document.querySelector("#myModal").style.padding = "";
//   document.querySelector("#myModal").setAttribute("aria-hidden", "true");
//   $('.modal-backdrop').remove();
//   document.querySelector("#myModal").classList.remove("show");
//   document.querySelector("body").classList.remove("modal-open");
//   document.querySelector("body").style = "none";
// }
function showStaffEdit(nv) {
  document.querySelector("#tknv").value = nv.account;
  document.querySelector("#name").value = nv.fullName;
  document.querySelector("#email").value = nv.email;
  document.querySelector("#password").value = nv.passWord;
  document.querySelector("#datepicker").value = nv.workingDays;
  document.querySelector("#luongCB").value = nv.basicSalary;
  document.querySelector("#chucvu").value = nv.position;
  document.querySelector("#gioLam").value = nv.workingHours;
}
