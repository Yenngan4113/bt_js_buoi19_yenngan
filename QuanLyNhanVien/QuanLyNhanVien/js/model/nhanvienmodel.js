function Staff(
  _account,
  _fullName,
  _email,
  _passWord,
  _workingDays,
  _basicSalary,
  _position,
  _workingHours
) {
  this.account = _account;
  this.fullName = _fullName;
  this.email = _email;
  this.passWord = _passWord;
  this.workingDays = _workingDays;
  this.basicSalary = _basicSalary;
  this.position = _position;
  this.workingHours = _workingHours;
  this.fullSalary = function () {
    if (this.position == 1) {
      return this.basicSalary * 3;
    } else if (this.position == 2) {
      return this.basicSalary * 2;
    } else {
      return this.basicSalary;
    }
  };
  this.type = function () {
    if (this.workingHours >= 192) {
      return "Nhân viên xuất sắc";
    } else if (this.workingHours >= 176) {
      return "Nhân viên giỏi";
    } else if (this.workingHours >= 160) {
      return "Nhân viên khá";
    } else {
      return "Nhân viên trung bình";
    }
  };
}
