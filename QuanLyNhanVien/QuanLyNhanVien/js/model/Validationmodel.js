function Validation() {
  this.kiemTraRong = function (idCheck, idError) {
    let inputValue = document.querySelector(idCheck).value.trim();
    if (inputValue) {
      document.querySelector(idError).innerText = "";
      return true;
    } else {
      document.querySelector(idError).style.display = "block";
      document.querySelector(idError).innerText = "Không được để rỗng";
      return false;
    }
  };
  this.checkIDhople = function () {
    let inputValue = document.querySelector("#tknv").value;
    if (inputValue.length >= 4 && inputValue.length <= 6) {
      document.querySelector("#tbTKNV").innerText = "";
      return true;
    } else {
      document.querySelector("#tbTKNV").style.display = "block";
      document.querySelector("#tbTKNV").innerText = "ID phải có từ 4-6 ký tự";
      return false;
    }
  };
  this.checkTrungID = function () {
    let inputValue = document.querySelector("#tknv").value;
    let viTri = staffList.findIndex(function (item) {
      return item.account == inputValue;
    });
    if (viTri == -1) {
      document.querySelector("#tbTKNV").innerText = "";
      return true;
    } else {
      document.querySelector("#tbTKNV").style.display = "block";
      document.querySelector("#tbTKNV").innerText = "ID bị trùng";
      return false;
    }
  };
  this.checkLetters = function () {
    let input = document.querySelector("#name").value;
    let letters =
      /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
    if (letters.test(input)) {
      document.querySelector("#tbTen").innerText = "";
      return true;
    } else {
      document.querySelector("#tbTen").style.display = "block";
      document.querySelector("#tbTen").innerText = "Tên không hợp lệ";
      return false;
    }
  };
  this.checkEmailHopLe = function () {
    let input = document.querySelector("#email").value;
    let checkemail =
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/;

    if (input.match(checkemail)) {
      document.querySelector("#tbEmail").innerText = "";
      return true;
    } else {
      document.querySelector("#tbEmail").style.display = "block";
      document.querySelector("#tbEmail").innerText = "Email không hợp lệ";
      return false;
    }
  };
  this.checkPassword = function () {
    let input = document.querySelector("#password").value;
    let passWord = /^(?=.*?[0-9])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d\w\W]{6,10}$/;
    if (input.match(passWord)) {
      document.querySelector("#tbMatKhau").innerText = "";
      return true;
    } else {
      document.querySelector("#tbMatKhau").style.display = "block";
      document.querySelector("#tbMatKhau").innerText =
        "Password phải có từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)";
      return false;
    }
  };
  this.checkDate = function () {
    let date = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
    let input = document.querySelector("#datepicker").value;
    if (date.test(input)) {
      document.querySelector("#tbNgay").innerText = "";
      return true;
    } else {
      document.querySelector("#tbNgay").style.display = "block";
      document.querySelector("#tbNgay").innerText = "Ngày định dạng mm/dd/yyyy";
      return false;
    }
  };
  this.checkSalary = function () {
    let input = document.querySelector("#luongCB").value;
    if (input >= 1e6 && input <= 20e6) {
      document.querySelector("#tbLuongCB").innerText = "";
      return true;
    } else {
      document.querySelector("#tbLuongCB").style.display = "block";
      document.querySelector("#tbLuongCB").innerText =
        "Lương căn bản không hợp lệ";
      return false;
    }
  };
  this.checkPosition = function () {
    let input = document.querySelector("#chucvu").value;
    console.log(input);
    if (input == "1" || input == "2" || input == "3") {
      document.querySelector("#inform").innerText = "";

      return true;
    } else {
      document.querySelector("#inform").style.display = "block";
      document.querySelector("#inform").innerText = "Vui lòng chọn chức vụ";
      return false;
    }
  };
  this.checkWorkingHours = function () {
    let input = document.querySelector("#gioLam").value;

    if (input >= 80 && input <= 200) {
      document.querySelector("#tbGiolam").innerText = "";
      return true;
    } else {
      document.querySelector("#tbGiolam").style.display = "block";
      document.querySelector("#tbGiolam").innerText =
        "Số giờ làm trong khoảng 80-200 giờ";
      return false;
    }
  };
}
