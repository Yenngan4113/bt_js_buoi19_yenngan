let staffList = [];
let validationStaff = new Validation();
// Save date to local storage
let STAFFLIST_STORAGE = "STAFFLIST_STORAGE";
function saveDataToStorage() {
  let staffListJSON = JSON.stringify(staffList);
  localStorage.setItem(STAFFLIST_STORAGE, staffListJSON);
}
const renderStaffList = function () {
  let staffListJSON = localStorage.getItem(STAFFLIST_STORAGE);
  if (staffListJSON) {
    staffList = JSON.parse(staffListJSON);
    console.log(staffList);
    staffList = staffList.map(function (item) {
      return new Staff(
        item.account,
        item.fullName,
        item.email,
        item.passWord,
        item.workingDays,
        item.basicSalary,
        item.position,
        item.workingHours
      );
    });
    showStaffList(staffList);
  }
};
renderStaffList();
function searchStaff(id, array) {
  return array.findIndex(function (item) {
    return item.account == id;
  });
}
let isUpdate = false;
let checkValidation = function () {
  isValidfullName =
    validationStaff.kiemTraRong("#name", "#tbTen") &&
    validationStaff.checkLetters();
  isValidEmail =
    validationStaff.kiemTraRong("#name", "#tbEmail") &&
    validationStaff.checkEmailHopLe();
  isValidPassword =
    validationStaff.kiemTraRong("#password", "#tbMatKhau") &&
    validationStaff.checkPassword();
  isValidDate =
    validationStaff.kiemTraRong("#datepicker", "#tbNgay") &&
    validationStaff.checkDate();
  isValidSalary =
    validationStaff.kiemTraRong("#luongCB", "#tbLuongCB") &&
    validationStaff.checkSalary();
  isValidPosition = validationStaff.checkPosition();
  isValidWorkingHours =
    validationStaff.kiemTraRong("#gioLam", "#tbGiolam") &&
    validationStaff.checkWorkingHours();
  if (isUpdate) {
    isValidTK =
      validationStaff.kiemTraRong("#tknv", "#tbTKNV") &&
      validationStaff.checkIDhople();
  } else {
    isValidTK =
      validationStaff.kiemTraRong("#tknv", "#tbTKNV") &&
      validationStaff.checkIDhople() &&
      validationStaff.checkTrungID();
  }
  isValid =
    isValidTK &&
    isValidfullName &&
    isValidEmail &&
    isValidPassword &&
    isValidDate &&
    isValidPosition &&
    isValidSalary &&
    isValidWorkingHours;
};
document.querySelector("#btnThem").addEventListener("click", function () {
  clearSpan();

  document.querySelector("#tknv").disabled = false;
  document.querySelector("#tknv").value = "";
  document.querySelector("#name").value = "";
  document.querySelector("#email").value = "";
  document.querySelector("#password").value = "";
  document.querySelector("#datepicker").value = "";
  document.querySelector("#luongCB").value = "";
  document.querySelector("#chucvu").value = "0";
  document.querySelector("#gioLam").value = "";
});
function clearSpan() {
  document.querySelector("#tbTKNV").innerText = "";
  document.querySelector("#tbTen").innerText = "";
  document.querySelector("#tbEmail").innerText = "";
  document.querySelector("#tbMatKhau").innerText = "";
  document.querySelector("#tbNgay").innerText = "";
  document.querySelector("#tbLuongCB").innerText = "";
  document.querySelector("#inform").innerText = "";
  document.querySelector("#tbGiolam").innerText = "";
}
document.querySelector("#btnThemNV").addEventListener("click", function () {
  isUpdate = false;
  checkValidation();
  if (isValid) {
    let newStaff = getInforFromInput();
    staffList.push(newStaff);
    saveDataToStorage();
    showStaffList(staffList);
  }
});
function deleteStaff(id) {
  let viTri = searchStaff(id, staffList);
  staffList.splice(viTri, 1);
  saveDataToStorage();
  renderStaffList();
}
function editInfor(id) {
  clearSpan();
  document.querySelector("#tknv").disabled = true;
  let viTri = searchStaff(id, staffList);
  console.log({ viTri });
  let editStaff = staffList[viTri];
  console.log(editStaff);
  showStaffEdit(editStaff);
}
document.querySelector("#btnCapNhat").addEventListener("click", function () {
  isUpdate = true;
  checkValidation();
  console.log(isValid);
  if (isValid) {
    let editedStaff = getInforFromInput();
    let viTri = searchStaff(editedStaff.account, staffList);
    if (viTri != -1) {
      staffList[viTri] = editedStaff;
      saveDataToStorage();
      renderStaffList();
    }
  }
});
// search staff by type
document.querySelector("#btnTimNV").addEventListener("click", function () {
  let staffTypeList = [];
  let searchType = document.querySelector("#searchStaff").value;
  if (searchType == "1") {
    staffTypeList = staffList.filter(function (item) {
      return item.workingHours >= 192;
    });
    showStaffList(staffTypeList);
  } else if (searchType == "2") {
    staffTypeList = staffList.filter(function (item) {
      return item.workingHours >= 176 && item.workingHours < 192;
    });
    showStaffList(staffTypeList);
  } else if (searchType == "3") {
    staffTypeList = staffList.filter(function (item) {
      return item.workingHours >= 160 && item.workingHours < 176;
    });
    showStaffList(staffTypeList);
  } else if (searchType == "4") {
    staffTypeList = staffList.filter(function (item) {
      return item.workingHours < 160;
    });
    showStaffList(staffTypeList);
  } else {
    showStaffList(staffList);
  }
});
